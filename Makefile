.PHONY: up

up:
		docker run -d --name demo_jenkins -p 8080:8080 -p 50000:50000 -v `pwd`/jenkins_home:/var/jenkins_home jenkins && docker exec -it -u root demo_jenkins /bin/bash -c "apt-get update && apt-get -y install gcc libffi-dev \
		python-pip  build-essential libssl-dev python-dev python-setuptools python-markupsafe python-paramiko python-yaml && \
		pip install ansible"
stop:
		docker kill demo_jenkins

clean:
		docker kill demo_jenkins ; docker rm demo_jenkins
