# PoC Project #

This project demonstrate how works Ansible with EC2 instance. Scripts will setup for you jenkins in docker (*For get jenkins jobs for aws - send request to administrator*) and you will be able to run provisioning jobs.

Jenkins will pull this repository and run playbooks from aws/
You will be able to install nginx and simple demo page from jenkins web interface 

##Prerequisites##

* ansible
* packer
* docker

## How it works ##
*  **cd to repo dir**
*  **make up** -- this wil up jenkins with ansible. Jenkins will be availeble at localhost:8080
*  **make stop** -- Stop Jenkins container
*  **make clean** -- remove image and stop container


##Usage with packer##
Packer help you with test your ansible playbooks and can crate AWS AMI, Docker Images, RKT, etc.

* **cd to repo dir**
* **cd packer_docker**
* **make build** --build docker image and run ansible playbooks
* **make run** -- run docker container from built image
* **make stop** -- stop container
* **make clean** -- stop and remove image